//
//  StreamingViewController.swift
//  DashCamPlayer
//
//  Created by Long Uni on 4/8/20.
//  Copyright © 2020 DashCam. All rights reserved.
//

import UIKit
import IJKMediaFramework
import RxSwift

class StreamingViewController: UIViewController {

    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var refreshButton: UIButton!
    var player: IJKFFMoviePlayerController!
    
    @IBOutlet weak var videoPlayerContent: UIView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var timer = Timer()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerPlayerEvents()
        playStream()
        
        timer = Timer.scheduledTimer(timeInterval: 60 * 30, target: self, selector: #selector(self.autoRefreshPlayer), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removePlayer()
        timer.invalidate()
    }
    
    func playStream() {
        
        print("StreamingViewController playStream")
        
        self.loadingIndicator.isHidden = false
        self.loadingIndicator.startAnimating()
        
        let disposable = AppDataManager.shared.getStreamingRTSP().subscribe { event in
        
            switch event {
            case .success(let response):
                
                print("get RTSP url: ", response)
                
                if !response.isEmpty {
                    self.addRTSPStream(rtsp: response)
                }
                
                break
            case .error(let error):
                print("error", error)
            }}
        
        _ = CompositeDisposable().insert(disposable)
    }
    
    func addRTSPStream(rtsp: String) -> Void {
        
        removePlayer()
        
        IJKFFMoviePlayerController.setLogLevel(k_IJK_LOG_DEBUG)
        IJKFFMoviePlayerController.setLogReport(true)
        let options = IJKFFOptions.byDefault()
        
        
        options?.setPlayerOptionIntValue(30, forKey: "max-fps")
        options?.setPlayerOptionIntValue(1, forKey: "framedrop")
        options?.setPlayerOptionIntValue(0, forKey: "start-on-prepared")
        options?.setPlayerOptionIntValue(0, forKey: "http-detect-range-support")
        options?.setPlayerOptionIntValue(48, forKey: "skip_loop_filter")
        options?.setPlayerOptionIntValue(0, forKey: "packet-buffering")
        options?.setPlayerOptionIntValue(2000000, forKey: "analyzeduration")
        options?.setPlayerOptionIntValue(25, forKey: "min-frames")
        options?.setPlayerOptionIntValue(1, forKey: "start-on-prepared")
        
        options?.setCodecOptionIntValue(8, forKey: "skip_frame")
        
        options?.setFormatOptionValue( "nobuffer", forKey: "fflags")
        options?.setFormatOptionValue( "8192", forKey: "probsize")
        options?.setFormatOptionIntValue(0, forKey: "auto_convert")
        options?.setFormatOptionIntValue(1, forKey: "reconnect")
        
        //        options?.setPlayerOptionIntValue(1, forKey: "videotoolbox")
        //        ijkMediaPlayer.setOption(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "dns_cache_clear", 1);
        
        
        // 帧速率(fps) （可以改，确认非标准桢率会导致音画不同步，所以只能设定为15或者29.97）
        //        options?.setPlayerOptionIntValue(Int64(29.97), forKey:"r")
        // -vol——设置音量大小，256为标准音量。（要设置成两倍音量时则输入512，依此类推
        //        options?.setPlayerOptionIntValue(512, forKey:"vol")
        
        self.player = IJKFFMoviePlayerController(contentURL: URL(string: rtsp), with: options)
        
        let autoresize = UIView.AutoresizingMask.flexibleWidth.rawValue |
            UIView.AutoresizingMask.flexibleHeight.rawValue
        self.player.view.autoresizingMask = UIView.AutoresizingMask(rawValue: autoresize)
        
        self.player.view.frame = self.view.bounds
        self.player.scalingMode = IJKMPMovieScalingMode.aspectFit
        self.player.shouldAutoplay = true
        self.videoPlayerContent.autoresizesSubviews = true
        self.view.addSubview(self.player.view)
        self.player.prepareToPlay()
        self.view.sendSubviewToBack(self.player.view)
        
    }
    
    @objc func autoRefreshPlayer() {
        
        print("StreamingViewController autoRefreshPlayer")
        playStream()
    }
    
    func removePlayer() -> Void {
        
        print("StreamingViewController removePlayer")
        
        if self.player != nil {
            self.player.stop()
            self.player.view.removeFromSuperview()
            self.player = nil
        }
    }
    
    func registerPlayerEvents() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePlayerEvents(_:)), name: NSNotification.Name(rawValue: "IJKMPMoviePlayerPlaybackDidFinishNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePlayerEvents(_:)), name: NSNotification.Name(rawValue: "IJKMPMoviePlayerFirstVideoFrameRenderedNotification"), object: nil)

    }
    
    // handle notification
    @objc func handlePlayerEvents(_ notification: NSNotification) {
        
        print("StreamingViewController noti", notification.name.rawValue)
        
        switch notification.name.rawValue {
        case "IJKMPMoviePlayerPlaybackDidFinishNotification":
            playStream()
            break
        case "IJKMPMoviePlayerFirstVideoFrameRenderedNotification":
            loadingIndicator.stopAnimating()
            loadingIndicator.isHidden = true
            break
        default:
            print()
        }

        
    }
    
    
    @IBAction func onButtonClicked(_ sender: UIButton) {
        
        print("StreamingViewController onButtonClicked")
        
        if sender == closeButton{
            self.dismiss(animated: false, completion: nil)
        } else if sender == refreshButton {
            playStream()
        }
        
    }
}

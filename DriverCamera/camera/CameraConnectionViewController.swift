//
//  CameraConnectionViewController.swift
//  DashCamPlayer
//
//  Created by Long Uni on 4/8/20.
//  Copyright © 2020 DashCam. All rights reserved.
//

import UIKit
import RxSwift

class CameraConnectionViewController: UIViewController {
    @IBOutlet weak var connectDeviceButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        connectDeviceButton.layer.cornerRadius = connectDeviceButton.frame.height/2
    }

    @IBAction func onButtonClicked(_ sender: Any) {
        
        let vc = StreamingViewController()
        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(vc, animated: true, completion: nil)
        
//        self.navigationController?.pushViewController(vc, animated: false)
    }
}

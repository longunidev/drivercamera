//
//  VideosViewController.swift
//  DriverCamera
//
//  Created by Long Uni on 4/12/20.
//  Copyright © 2020 DashCam. All rights reserved.
//

import AVFoundation
import UIKit
import Nuke
import RxSwift

class VideosViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let NumberOfColumn = 3;
    
    @IBOutlet weak var thumbnailGrids: UICollectionView!
    var refresher:UIRefreshControl!
    
    var groupOfMedium = [[Media]]()
    
    var isAlreadyLoaded: Bool = false

    public var mediaType: MediaType = .NONE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("VideosViewController viewDidLoad type ", mediaType)
        
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = UIColor.red
        self.thumbnailGrids.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.red
        self.refresher.addTarget(self, action: #selector(loadMedium), for: .valueChanged)
        self.thumbnailGrids.addSubview(refresher)
        self.thumbnailGrids.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 120, right: 0);

        
        thumbnailGrids.register(UINib(nibName: "ThumbnailViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "ThumbnailViewCell")
        
        thumbnailGrids.register(UINib(nibName: "SectionHeader", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "SectionHeader")
        
        thumbnailGrids.register(UINib(nibName: "SectionHeader", bundle: nil), forCellWithReuseIdentifier: "SectionHeader")
        
        thumbnailGrids.dataSource = self
        thumbnailGrids.delegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !isAlreadyLoaded {
            loadMedium()
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return groupOfMedium[section].count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return groupOfMedium.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        
        if kind ==  UICollectionView.elementKindSectionHeader {
            if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeader", for: indexPath) as? SectionHeader {
                
                if let firstMedia = groupOfMedium[indexPath.section].first {
                    sectionHeader.sectionHeaderLabel.text = firstMedia.takenDate
                    
                } else {
                    sectionHeader.sectionHeaderLabel.text = ""
                }
                
                return sectionHeader
            }
        }
        
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThumbnailViewCell", for: indexPath) as! ThumbnailViewCell
        
        let subGroupOfMedium = groupOfMedium[indexPath.section]
        
        if indexPath.row < subGroupOfMedium.count {
            
            if let thumbnailURL = subGroupOfMedium[indexPath.row].thumbnailURL {
                let options = ImageLoadingOptions(
                  placeholder: UIImage(named: "dark-moon"),
                  transition: .fadeIn(duration: 0.5)
                )
                Nuke.loadImage(with: thumbnailURL, options: options, into: cell.thumbnailImageView)
            }
            
            cell.videoDurationLabel.text = subGroupOfMedium[indexPath.row].time
            
            if mediaType == .PHOTO {
                cell.playIconImageView.isHidden = true
            } else {
                cell.playIconImageView.isHidden = false
            }
            
        }
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let cellWidth = collectionView.frame.size.width/3.2
        
        return CGSize(width: cellWidth, height: 0.8*cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let subGroupOfMedium = groupOfMedium[indexPath.section]
        
        if indexPath.row < subGroupOfMedium.count {
            
            let vc = ViewDetailViewController()
            vc.media = subGroupOfMedium[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func loadMedium() -> Void {
        
        self.beginRefreshing()
        self.thumbnailGrids.restore()
        AppDataManager.shared.stopRecording(url: "")
        
        var GET_MEDIUM_URL = ""
        var emptyMessage = ""
        if mediaType == .PHOTO {
            GET_MEDIUM_URL = AppAPIHelper.GET_PHOTOS_URL
            emptyMessage = "No photos"
        } else if mediaType == .VIDEO {
            GET_MEDIUM_URL = AppAPIHelper.GET_VIDEOS_URL
            emptyMessage = "No videos"
        }
        
        let disposable = AppDataManager.shared.getMedia(url: GET_MEDIUM_URL).subscribe { event in
            
            switch event {
            case .success(let response):
                
                print("response photo: ", response)
                
                self.groupOfMedium = response
                
                DispatchQueue.main.async {
                    self.thumbnailGrids.reloadData()
                }
                self.endRefreshing()
                
                break
            case .error(let error):
                print("loadMedium getMedia error", error)
                self.groupOfMedium = [[]]
                self.endRefreshing()
                DispatchQueue.main.async {
                    self.thumbnailGrids.setEmptyMessage(emptyMessage)
                    self.thumbnailGrids.reloadData()
                }
                break
            }}
        _ = CompositeDisposable().insert(disposable)
        isAlreadyLoaded = true
    }
    
    func getVideoDuration(videoURL: URL) -> String {
        
        let asset = AVURLAsset(url: videoURL)
        let durationInSeconds = asset.duration.seconds
        
        return "\(durationInSeconds)"
    }
    
    func beginRefreshing() {
        if let _ = self.refresher {
            if self.refresher.isRefreshing {
                self.refresher.endRefreshing()
            }
            
            self.refresher.beginRefreshing()
        }
    }
    
    func endRefreshing() {
        AppDataManager.shared.startRecording(url: "")
        if let _ = self.refresher {
            DispatchQueue.main.async {
                self.refresher.endRefreshing()
            }
        }
    }
}

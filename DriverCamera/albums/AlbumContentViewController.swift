//
//  AlbumContentViewController.swift
//  DriverCamera
//
//  Created by Long Uni on 4/19/20.
//  Copyright © 2020 DashCam. All rights reserved.
//

import UIKit
import Foundation
import Swift_PageMenu

class AlbumContentViewController: PageMenuController {
    
    @IBOutlet weak var customNavigationBar: UINavigationBar!
    
    let photosVC = VideosViewController()
    let videosVC = VideosViewController()

    var controllers = [UIViewController]();
    
    var titles = ["PHOTO", "VIDEO"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        photosVC.mediaType = .PHOTO
        videosVC.mediaType = .VIDEO
        
        controllers = [photosVC, videosVC]

        self.delegate = self
        self.dataSource = self
    }

}

extension AlbumContentViewController: PageMenuControllerDataSource {

    func viewControllers(forPageMenuController pageMenuController: PageMenuController) -> [UIViewController] {
        return controllers
    }

    func menuTitles(forPageMenuController pageMenuController: PageMenuController) -> [String] {
        return self.titles
    }

    func defaultPageIndex(forPageMenuController pageMenuController: PageMenuController) -> Int {
        return 0
    }
}

extension AlbumContentViewController: PageMenuControllerDelegate {

    func pageMenuController(_ pageMenuController: PageMenuController, didScrollToPageAtIndex index: Int, direction: PageMenuNavigationDirection) {
        // The page view controller will begin scrolling to a new page.
        print("didScrollToPageAtIndex index:\(index)")
    }

    func pageMenuController(_ pageMenuController: PageMenuController, willScrollToPageAtIndex index: Int, direction: PageMenuNavigationDirection) {
        // The page view controller scroll progress between pages.
        print("willScrollToPageAtIndex index:\(index)")
    }

    func pageMenuController(_ pageMenuController: PageMenuController, scrollingProgress progress: CGFloat, direction: PageMenuNavigationDirection) {
        // The page view controller did complete scroll to a new page.
    }

    func pageMenuController(_ pageMenuController: PageMenuController, didSelectMenuItem index: Int, direction: PageMenuNavigationDirection) {
        print("didSelectMenuItem index: \(index)")
    }
}

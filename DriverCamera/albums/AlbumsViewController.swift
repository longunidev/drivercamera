//
//  AlbumsViewController.swift
//  DriverCamera
//
//  Created by Long Uni on 4/12/20.
//  Copyright © 2020 DashCam. All rights reserved.
//

import UIKit
import RxSwift

class AlbumsViewController: UIViewController, UITabBarControllerDelegate {
    @IBOutlet weak var contentView: UIView!
    
    let albumsVC = AlbumContentViewController(options: RoundRectPagerOption())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.addChild(albumsVC)
        self.contentView.addSubview(albumsVC.view)

        albumsVC.didMove(toParent: self)
        
    }
    
}

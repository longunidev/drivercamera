//
//  RoundRectPagerOption.swift
//  DriverCamera
//
//  Created by Long Uni on 4/19/20.
//  Copyright © 2020 DashCam. All rights reserved.
//

import Foundation
import Swift_PageMenu

struct RoundRectPagerOption: PageMenuOptions {

    var isInfinite: Bool = false

    var tabMenuPosition: TabMenuPosition = .top

    var menuItemSize: PageMenuItemSize {
        return .sizeToFit(minWidth: UIScreen.main.bounds.width/2 - 5, height: 44)
    }

    var menuTitleColor: UIColor {
        return .gray
    }

    var menuTitleSelectedColor: UIColor {
        return .black
    }

    var menuCursor: PageMenuCursor {
        return .roundRect(rectColor: .white, cornerRadius: 10, height: 44, borderWidth: nil, borderColor: nil)
    }

    var font: UIFont {
        return UIFont.systemFont(ofSize: 18)
    }

    var menuItemMargin: CGFloat {
        return 0
    }

    var tabMenuBackgroundColor: UIColor {
        return UIColor.white
    }

    var tabMenuContentInset: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
    }

    public init(isInfinite: Bool = false, tabMenuPosition: TabMenuPosition = .top) {
        self.isInfinite = isInfinite
        self.tabMenuPosition = tabMenuPosition
    }
}


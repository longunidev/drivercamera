//
//  ViewDetailViewController.swift
//  DriverCamera
//
//  Created by Long Uni on 4/13/20.
//  Copyright © 2020 DashCam. All rights reserved.
//

import UIKit
import Nuke
import AVKit
import AVFoundation
import IJKMediaFramework

class ViewDetailViewController: UIViewController {
    
    @IBOutlet weak var customNavigationBar: UINavigationBar!
    
    var media: Media?
    
    var videoPlayer = AVPlayer()
    var avPlayerViewController = AVPlayerViewController()
    var player: IJKFFMoviePlayerController!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.customNavigationBar.topItem?.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(self.goBack))
        
        if let media = self.media {
            
            if media.mediaType == .PHOTO {
                setUpPhotoView()
            } else if media.mediaType == .VIDEO {
                setUpVideoView()
            }
            
            // Find title name
            let splits = media.mediaURL?.absoluteString.split{$0 == "/"}.map(String.init)
            if let fileName = splits?.last {
                self.customNavigationBar.topItem?.title = fileName
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        removeIJKPlayer()
    }
    
    func setUpPhotoView() -> Void {
        
        var imageView : UIImageView
        imageView  = UIImageView(frame: self.view.frame);
        
        let autoresize = UIView.AutoresizingMask.flexibleWidth.rawValue |
            UIView.AutoresizingMask.flexibleHeight.rawValue
        imageView.autoresizingMask = UIView.AutoresizingMask(rawValue: autoresize)
        imageView.contentMode = .scaleAspectFit

        self.view.autoresizesSubviews = true
        
        self.view.addSubview(imageView)
        
        if let mediaURL = self.media?.mediaURL {
            Nuke.loadImage(with: mediaURL, into: imageView)
        }
    }
    
    func setUpVideoView() -> Void {
        
        if let mediaURL = self.media?.mediaURL {
            self.playVideo(url: mediaURL)
        }
            
    }
    func playVideo(url: URL) {
        self.videoPlayer = AVPlayer(url: url)
        self.avPlayerViewController = AVPlayerViewController()
        self.avPlayerViewController.player = self.videoPlayer
        self.avPlayerViewController.view.frame = self.view.frame
        self.addChild(avPlayerViewController)
        if let avView = avPlayerViewController.view {
            avView.backgroundColor = .white
            self.view.addSubview(avView)
            self.view.sendSubviewToBack(avView)
        }
        
    }
    
    func playVideo2(url: URL) {
        
        removeIJKPlayer()
        
        IJKFFMoviePlayerController.setLogLevel(k_IJK_LOG_DEBUG)
        IJKFFMoviePlayerController.setLogReport(true)
        let options = IJKFFOptions.byDefault()
        
        options?.setPlayerOptionIntValue(30, forKey: "max-fps")
        options?.setPlayerOptionIntValue(1, forKey: "framedrop")
        options?.setPlayerOptionIntValue(0, forKey: "start-on-prepared")
        options?.setPlayerOptionIntValue(0, forKey: "http-detect-range-support")
        options?.setPlayerOptionIntValue(48, forKey: "skip_loop_filter")
        options?.setPlayerOptionIntValue(0, forKey: "packet-buffering")
        options?.setPlayerOptionIntValue(2000000, forKey: "analyzeduration")
        options?.setPlayerOptionIntValue(25, forKey: "min-frames")
        options?.setPlayerOptionIntValue(1, forKey: "start-on-prepared")
        
        options?.setCodecOptionIntValue(8, forKey: "skip_frame")
        
        options?.setFormatOptionValue( "nobuffer", forKey: "fflags")
        options?.setFormatOptionValue( "8192", forKey: "probsize")
        options?.setFormatOptionIntValue(0, forKey: "auto_convert")
        options?.setFormatOptionIntValue(1, forKey: "reconnect")
        
        self.player = IJKFFMoviePlayerController(contentURL: url, with: options)
        
        let autoresize = UIView.AutoresizingMask.flexibleWidth.rawValue |
            UIView.AutoresizingMask.flexibleHeight.rawValue
        self.player.view.autoresizingMask = UIView.AutoresizingMask(rawValue: autoresize)
        
        self.player.view.frame = self.view.bounds
        self.player.scalingMode = IJKMPMovieScalingMode.aspectFit
        self.player.shouldAutoplay = true
        self.view.addSubview(self.player.view)
        self.player.prepareToPlay()
        self.view.sendSubviewToBack(self.player.view)
    }
    
    func removeIJKPlayer() -> Void {
        
        print("removeIJKPlayer")
        
        if self.player != nil {
            self.player.stop()
            self.player.view.removeFromSuperview()
            self.player = nil
        }
    }
    
    @objc func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
}

//
//  ThumbnailViewCell.swift
//  DriverCamera
//
//  Created by Long Uni on 4/12/20.
//  Copyright © 2020 DashCam. All rights reserved.
//

import UIKit

class ThumbnailViewCell: UICollectionViewCell {

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var playIconImageView: UIImageView!
    @IBOutlet weak var videoDurationLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        videoDurationLabel.translatesAutoresizingMaskIntoConstraints = false
//        let bottom = NSLayoutConstraint(item: videoDurationLabel!, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: thumbnailImageView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -10)
//
//        let leading = NSLayoutConstraint(item: videoDurationLabel!, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: thumbnailImageView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
//        
//        self.addConstraints([bottom, leading])

        thumbnailImageView.layer.borderWidth = 5
        thumbnailImageView.layer.borderColor = UIColor.red.cgColor

    }

}

//
//  SectionHeader.swift
//  DriverCamera
//
//  Created by Long Uni on 4/20/20.
//  Copyright © 2020 DashCam. All rights reserved.
//

import UIKit

class SectionHeader: UICollectionReusableView {

    @IBOutlet weak var sectionHeaderLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

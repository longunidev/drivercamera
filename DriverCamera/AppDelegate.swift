//
//  AppDelegate.swift
//  DriverCamera
//
//  Created by Long Uni on 4/12/20.
//  Copyright © 2020 DashCam. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    let rootViewController = ViewController()
    let camerConnectionVC = CameraConnectionViewController()
    let albumsVC = AlbumsViewController()
    let settingsVC = SettingsViewController()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setUpTabbar()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func setUpTabbar() -> Void {
        
        self.camerConnectionVC.tabBarItem = UITabBarItem(title: "Camera", image: UIImage(named: "ic_camera"), selectedImage: UIImage(named: "ic_camera"))
        self.albumsVC.tabBarItem = UITabBarItem(title: "Albums", image: UIImage(named: "ic_video"), selectedImage: UIImage(named: "ic_video"))
        self.settingsVC.tabBarItem = UITabBarItem(title: "Settings", image: UIImage(named: "ic_settings"), selectedImage: UIImage(named: "ic_settings"))
        
        
        self.rootViewController.viewControllers = [camerConnectionVC, albumsVC, settingsVC]
        
        let navigationController = UINavigationController(rootViewController: self.rootViewController)
        navigationController.navigationBar.isHidden = true
        self.window?.rootViewController = navigationController
        self.window?.backgroundColor = UIColor.white
        self.window?.makeKeyAndVisible()
        
    }

}


//
//  AppDataManager.swift
//  DriverCamera
//
//  Created by Long Uni on 4/12/20.
//  Copyright © 2020 DashCam. All rights reserved.
//

import Foundation
import RxSwift

class AppDataManager: DataManager {
    
    static let shared = AppDataManager(apiHelperProtocol: AppAPIHelper())

    private var apiHelperProtocol: APIHelperProtocol
    
    private init(apiHelperProtocol: APIHelperProtocol) {
        self.apiHelperProtocol = apiHelperProtocol
    }
    
    //MARK: APIHelper
    
    func initialize() {
        self.apiHelperProtocol.initialize()
    }
    
    func startRecording(url: String) {
        self.apiHelperProtocol.startRecording(url: url)
    }
    
    func stopRecording(url: String) {
        self.apiHelperProtocol.stopRecording(url: url)
    }
    
    func getVideos(url: String) -> Single<[String]> {
        return self.apiHelperProtocol.getVideos(url: url)
    }
    
    func getPhotos(url: String) -> Single<[String]> {
        return self.apiHelperProtocol.getPhotos(url: url)
    }
    
    func getMedia(url: String) -> Single<[[Media]]> {
        return self.apiHelperProtocol.getMedia(url: url)
    }
    
    func getStreamingRTSP() -> Single<String> {
        return self.apiHelperProtocol.getStreamingRTSP()
    }
}

//
//  AppAPIHelper.swift
//  DriverCamera
//
//  Created by Long Uni on 4/12/20.
//  Copyright © 2020 DashCam. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

class AppAPIHelper: APIHelperProtocol {
    
    public let BASE_HOST = "192.168.0.1"
    
    public static let START_RECORDING_COMMAND_URL = "http://192.168.0.1/cgi-bin/hisnet/workmodecmd.cgi?-cmd=start"
    
    public static let STOP_RECORDING_COMMAND_URL = "http://192.168.0.1/cgi-bin/hisnet/workmodecmd.cgi?-cmd=stop"
    
    public static let GET_PHOTOS_URL = "http://192.168.0.1/cgi-bin/hisnet/getdirfilelist.cgi?-dir=photo&-start=0&-end=1000"
    
    public static let GET_VIDEOS_URL = "http://192.168.0.1/cgi-bin/hisnet/getdirfilelist.cgi?-dir=front_norm&-start=0&-end=1000"
    
    private var deviceIPAddress = ""
    
    func initialize() {}
    
    func startRecording(url: String) {
        
        // 1
        let request = AF.request("http://\(BASE_HOST)/cgi-bin/hisnet/workmodecmd.cgi?-cmd=start")
        // 2
        request.responseString { (response) in
            switch response.result{
            case .success(let data):
                
                    print("startRecording data", data)
                break
            case .failure(let error):
                
                print("startRecording error", error)
                break
            }}
    }
    
    func stopRecording(url: String) {
        
        // 1
        let request = AF.request("http://\(self.BASE_HOST)/cgi-bin/hisnet/workmodecmd.cgi?-cmd=stop")
        // 2
        request.responseString { (response) in
            switch response.result{
            case .success(let data):
                
                    print("stopRecording data", data)
                break
            case .failure(let error):
                
                print("stopRecording error", error)
                break
            }}
    }
    
    
    func getVideos(url: String) -> Single<[String]> {
        
        print("getVideos begins")
        
        return Single<[String]>.create(subscribe: { single -> Disposable in
            
            
            // 1
            let request = AF.request("http://\(self.BASE_HOST)/cgi-bin/hisnet/getdirfilelist.cgi?-dir=front_norm&-start=0&-end=1000")
            // 2
            request.responseString { (response) in
                
                print("getVideos ends", response.response)
                
                switch response.result{
                case .success(let data):
                    let videoURLs = data.split{$0 == ";"}.map(String.init)
                    
                    single(.success(videoURLs))
                    
                    break
                case .failure(let error):
                    single(.error(error))
                    break
                }}
            
            
            
            return Disposables.create {}
        })
    }
    
    func getPhotos(url: String) -> Single<[String]> {
        
        print("getPhotos begins")
        
        return Single<[String]>.create(subscribe: { single -> Disposable in
            
            
            // 1
            let request = AF.request("http://\(self.BASE_HOST)/cgi-bin/hisnet/getdirfilelist.cgi?-dir=photo&-start=0&-end=1000")
            // 2
            request.responseString { (response) in
                switch response.result{
                case .success(let data):
                    let photosURLs = data.split{$0 == ";"}.map(String.init)
                    
                    single(.success(photosURLs))
                    
                    break
                case .failure(let error):
                    print("getPhotos error", error)
                    single(.error(error))
                    break
                }}
            
            
            
            return Disposables.create {}
        })
    }
    
    func getMedia(url: String) -> Single<[[Media]]> {
        return Single<[[Media]]>.create(subscribe: { single -> Disposable in
            
            let request = AF.request(url)
            request.responseString { (response) in
                switch response.result{
                case .success(let data):
                    
                    print("getMedia data", data)
                    
                    let mediaURLs = data.split{$0 == ";"}.map(String.init)
                    
                    var medium = [Media]()
                    
                    for url in mediaURLs {
                        if !url.isEmpty {
                            if let media = self.creatMediaFromPath(mediaPath: url) {
                                medium.append(media)
                            }
                        }
                    }
                    
                    let groupOfMedium = Array(Dictionary(grouping: medium) { $0.takenDate }.values)
                    
                    let sortedByDateGroup = groupOfMedium.sorted { (group1: [Media], group2: [Media]) -> Bool in
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"// yyyy-MM-dd
                        
                        if let media1 = group1.first, let media2 = group2.first {
                            
                            if let date1 = dateFormatter.date(from: media1.takenDate), let date2 = dateFormatter.date(from: media2.takenDate) {
                                
                                return date1.compare(date2) == .orderedDescending
                            }
                        }
                        
                        return false
                    }
                    
                    single(.success(sortedByDateGroup))
                    
                    break
                case .failure(let error):
                    print("getMedia error", error)
                    single(.error(error))
                    break
                }}
            
            return Disposables.create {}
        })
    }
    
    func getStreamingRTSP() -> Single<String> {
        return Single<String>.create(subscribe: { single -> Disposable in
            
            
            // 1
            let request2 = AF.request("http://\(self.BASE_HOST)/cgi-bin/hisnet/getcamchnl.cgi?&-camid=0")
            // 2
            request2.responseString { (response) in
                switch response.result{
                case .success(let data):
                    
                    let results = self.matches(for: "[0-9]", in: data)
                    
                    if results.count > 0 {
                        let camChannel = results[0]
                        
                        single(.success("rtsp://192.168.0.1:554/livestream/\(camChannel)"))
                    } else {
                        
                        single(.success("rtsp://192.168.0.1:554/livestream/5"))
                    }
                    break
                case .failure(let error):
                    
                    print("cam id error", error)
                    
                    single(.error(error))
                    break
                }}
            
            
            return Disposables.create {}
        })
    }
    
    func matches(for regex: String, in text: String) -> [String] {

        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    func getIPAddress() -> String? {
            var address: String?
            var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
            if getifaddrs(&ifaddr) == 0 {
                var ptr = ifaddr
                while ptr != nil {
                    defer { ptr = ptr?.pointee.ifa_next }

                    let interface = ptr?.pointee
                    let addrFamily = interface?.ifa_addr.pointee.sa_family
                    if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {

                        if let name: String = String(cString: (interface?.ifa_name)!), name == "en0" {
                            var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                            getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                            address = String(cString: hostname)
                        }
                    }
                }
                freeifaddrs(ifaddr)
            }
            return address
    }
    
    func getRootIPAddress(ipAddress: String) -> String {
        

        var arr = ipAddress.components(separatedBy: ".")
        arr.removeLast()
        let newstr = arr.joined(separator: ".") + ".1"
        
        return newstr
    }
    
    private func creatMediaFromPath(mediaPath: String) -> Media? {
        
        print("creatMediaFromPath", mediaPath)
        
        
        var splits = [String]()
        
        splits = mediaPath.split{$0 == "/"}.map(String.init)
        
        if splits.count == 0 {
            return nil
        }
        
        let fileName = splits.last
        
        if fileName == nil || fileName!.isEmpty {
            return nil
        }
        
        splits = (fileName?.split{$0 == "_"}.map(String.init))!
        
        if splits.count < 4 {
            return nil
        }
        
        let year = splits[0]
        let month = splits[1]
        let day = splits[2]
        let time = splits[3]

        var media = Media()
        media.takenDate = "\(year)-\(month)-\(day)"
        media.time = time.separate(every:2, with: ":")
        
        var mediaURLString = ""
        var mediaThumbnailURLString = ""
        
        if mediaPath.hasSuffix("MP4") {
            media.mediaType = .VIDEO
            mediaURLString = "http://\(BASE_HOST)/\(mediaPath)"
            mediaThumbnailURLString = "http://\(BASE_HOST)/\(mediaPath.replacingOccurrences(of: "MP4", with: "THM"))"
        } else {
            media.mediaType = .PHOTO
            mediaURLString = "http://\(BASE_HOST)/\(mediaPath)"
            mediaThumbnailURLString = "http://\(BASE_HOST)/\(mediaPath)"
        }
        
        if !mediaURLString.isEmpty {
            media.mediaURL = URL(string: mediaURLString)
        }
        
        if !mediaThumbnailURLString.isEmpty {
            media.thumbnailURL = URL(string: mediaThumbnailURLString)
        }
        
        return media
    }
    
    private func createMediaGroupedByDate(listOfMedia: [Media]) -> [[Media]]{
        
        let objectGroups = Array(Dictionary(grouping: listOfMedia) { $0.takenDate }.values)
        
        return objectGroups
    }
}

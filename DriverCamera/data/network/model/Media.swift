//
//  Media.swift
//  DriverCamera
//
//  Created by Long Uni on 4/20/20.
//  Copyright © 2020 DashCam. All rights reserved.
//

import Foundation

enum MediaType {
    case NONE, PHOTO, VIDEO, URGENT
}

struct Media {
    
    var mediaType: MediaType = .NONE
    var thumbnailURL: URL?
    var mediaURL: URL?
    var takenDate = ""
    var time = ""
    
    public init(){}
}

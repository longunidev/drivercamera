//
//  APIHelperProtocol.swift
//  DriverCamera
//
//  Created by Long Uni on 4/12/20.
//  Copyright © 2020 DashCam. All rights reserved.
//

import Foundation
import RxSwift

protocol APIHelperProtocol {
    
    func initialize()
    
    func startRecording(url: String)
    
    func stopRecording(url: String)
    
    func getVideos(url: String) -> Single<[String]>
    
    func getPhotos(url: String) -> Single<[String]>
    func getMedia(url: String) -> Single<[[Media]]>
    
    func getStreamingRTSP() -> Single<String>
}
